﻿using System;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    static NetworkManager instance;
    [SerializeField] string gameVersion = "v0.1";

    event Action onNetworkReady;
    event Action<string> onFailedToJoin;
    event Action onGameRoomLeft;

    [Header("Room Data")]
    [SerializeField] string roomName;
    [SerializeField] int maxPlayersCount;

    //Properties
    public static NetworkManager Instance => instance;
    public event Action OnNetworkReady
    {
        add => onNetworkReady += value;
        remove => onNetworkReady -= value;
    }
    public event Action<string> OnFailedToJoin
    {
        add => onFailedToJoin += value;
        remove => onFailedToJoin -= value;
    }
    public event Action OnGameRoomLeft
    {
        add => onGameRoomLeft += value;
        remove => onGameRoomLeft -= value;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        PhotonNetwork.GameVersion = gameVersion;
        PhotonNetwork.ConnectUsingSettings();
    }

    public void JoinRoom(string userName)
    {
        PhotonNetwork.NickName = userName;
        PhotonNetwork.JoinOrCreateRoom(roomName, new Photon.Realtime.RoomOptions() { MaxPlayers = (byte)maxPlayersCount }, null);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    #region Connection Callbacks

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        onNetworkReady?.Invoke();
        //Debug.Log("Connected to master");
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("created room: " + PhotonNetwork.CurrentRoom.Name);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        PhotonNetwork.LoadLevel(1);
        Debug.Log(PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        onFailedToJoin?.Invoke(message);
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        onGameRoomLeft?.Invoke();
    }

    #endregion
}
