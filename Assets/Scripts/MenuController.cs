﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private string versionName = "0.1";
    public GameObject userNameMenu;
    public GameObject ConnectPanel;

    public InputField userNameInput;
    public InputField CreateGameInput;
    public InputField joinGameInput;

    public GameObject StartButton;

    private void Start()
    {
        userNameMenu.SetActive(true);
    }

    public void ChangeUserNameInput()
    {
        if (userNameInput.text.Length >= 3)
        {
            StartButton.SetActive(true);
        }
        else
        {
            StartButton.SetActive(false);
        }
    }
}
