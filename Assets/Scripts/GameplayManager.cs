﻿using UnityEngine;
using Cinemachine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System;

public class GameplayManager : MonoBehaviour
{
    static GameplayManager instance;

    [SerializeField] Transform[] spawningPositions;
    [SerializeField] CinemachineVirtualCamera vCam;
    [SerializeField] GameObject deathParticles;

    //Properties
    public static GameplayManager Instance => instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        NetworkManager.Instance.OnGameRoomLeft += OnGameRoomLeft;
        PhotonNetwork.Instantiate("Player", GetRandomSpawningPosition(), Quaternion.identity);
    }

    public void AssignVCamTarget(Transform player)
    {
        vCam.Follow = player;
        vCam.LookAt = player;
    }

    public Vector3 GetRandomSpawningPosition()
    {
        return spawningPositions[UnityEngine.Random.Range(0, spawningPositions.Length)].position;
    }

    public void SpawnDeathParticles(Vector3 position)
    {
        Instantiate(deathParticles, position, Quaternion.identity);
    }

    private void OnGameRoomLeft()
    {
        SceneManager.LoadScene(0);
    }

    private void OnDestroy()
    {
        NetworkManager.Instance.OnGameRoomLeft -= OnGameRoomLeft;
    }
}
