﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class LobbyUIManager : MonoBehaviour
{
    [SerializeField] GameObject connectingPanel;
    [SerializeField] GameObject roomDataPanel;
    [SerializeField] TMP_InputField txt_userName;
    [SerializeField] Button btn_joinRoom;
    [SerializeField] TextMeshProUGUI txt_error;
    [SerializeField] float errorDisplayDuration;

    private void Start()
    {
        NetworkManager.Instance.OnNetworkReady += OnNetworkConnected;
        NetworkManager.Instance.OnFailedToJoin += OnFailedToJoin;
        roomDataPanel.SetActive(false);
        connectingPanel.SetActive(true);
        btn_joinRoom.interactable = false;
        txt_error.gameObject.SetActive(false);
    }

    private void OnNetworkConnected()
    {
        connectingPanel.SetActive(false);
        roomDataPanel.SetActive(true);
    }

    public void OnUserNameUpdated()
    {
        btn_joinRoom.interactable = !string.IsNullOrEmpty(txt_userName.text);
    }

    public void JoinRoom()
    {
        NetworkManager.Instance.JoinRoom(txt_userName.text);
    }

    void OnFailedToJoin(string msg)
    {
        StartCoroutine(DisplayError(msg));
    }

    private void OnDestroy()
    {
        NetworkManager.Instance.OnNetworkReady -= OnNetworkConnected;
        NetworkManager.Instance.OnFailedToJoin -= OnFailedToJoin;
    }

    IEnumerator DisplayError(string msg)
    {
        txt_error.text = msg;
        txt_error.gameObject.SetActive(true);
        yield return new WaitForSeconds(errorDisplayDuration);
        txt_error.gameObject.SetActive(false);
    }
}
