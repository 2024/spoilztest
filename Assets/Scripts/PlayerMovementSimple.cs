﻿using System.Collections;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PlayerMovementSimple : MonoBehaviourPun, IPunObservable
{
    [Header("Movement")]
    [SerializeField] private int movementSpeed;
    [SerializeField] private LayerMask collisionLayer;

    private Vector2 _input;
    private RaycastHit2D collisionRaycast;

    [Header("UI")]
    [SerializeField] RectTransform uiPanel;
    [SerializeField] Vector3 uiPanelOffset;
    [SerializeField] TextMeshProUGUI txt_playerName;
    [SerializeField] GameObject[] lives;
    [SerializeField] GameObject localPlayerMarker;

    //RPCs
    const string RPC_KillParticles = "RPCSpawnKillParticles";

    //Applying Damage
    float health;
    Collider2D col;
    Animator anim;
    int AnimHash_TakeDmg;
    int AnimHash_Invulnerable;
    [SerializeField] float invulnerabilityDuration;
    [SerializeField] float deadDuration;
    bool isInvulnerable;
    bool isDead;

    private void Start()
    {
        col = GetComponent<BoxCollider2D>();
        anim = GetComponent<Animator>();
        AnimHash_TakeDmg = Animator.StringToHash("TakeDamage");
        AnimHash_Invulnerable = Animator.StringToHash("Invulnerable");

        Init();
    }

    private void Init()
    {
        txt_playerName.text = photonView.Owner.NickName;
        health = lives.Length;
        localPlayerMarker.SetActive(photonView.IsMine);
        uiPanel.SetParent(null);

        if (photonView.IsMine)
        {
            GameplayManager.Instance.AssignVCamTarget(transform); 
            localPlayerMarker.transform.SetParent(null);
        }

        isInvulnerable = true;
        StartCoroutine(StartInvulnerability());
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            if (!isDead)
            {
                GetPlayerInput();  
            }
            else
            {
                _input = Vector2.zero;
            }
            localPlayerMarker.transform.position = transform.position;
            //localPlayerMarker.transform.up = Vector3.up;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            NetworkManager.Instance.LeaveRoom();
        }

        //Fix UI panel
        uiPanel.position = transform.position + uiPanelOffset;
        //uiPanel.up = Vector3.up;
    }

    private void FixedUpdate()
    {
        if (photonView.IsMine)
        {
            MovePlayer();
            if (_input.sqrMagnitude != 0)
                RotatePlayer(); 
        }
    }

    private void GetPlayerInput()
    {
        _input.x = Input.GetAxisRaw("Horizontal");
        _input.y = Input.GetAxisRaw("Vertical");
    }

    private void MovePlayer()
    {
        collisionRaycast = Physics2D.Raycast(transform.position, _input.normalized, Time.deltaTime * movementSpeed * 2, collisionLayer);
        if(collisionRaycast.collider == null) //Not colliding with anything
        {
            transform.Translate(_input.normalized * Time.deltaTime * movementSpeed, Space.World); //Normalize coz diagonal movement is faster than horizontal & vertical
        }
    }

    private void RotatePlayer()
    {
        float angle = Mathf.Atan2(_input.y, _input.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void UpdateHealthUI()
    {
        for (int i = 0; i < lives.Length; i++)
        {
            lives[i].SetActive(i < health);
        }
    }

    [PunRPC]
    void RPCSpawnKillParticles()
    {
        GameplayManager.Instance.SpawnDeathParticles(transform.position);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isInvulnerable && collision.gameObject.CompareTag("Player"))
        {
            anim.SetTrigger(AnimHash_TakeDmg);
            StartCoroutine(StartInvulnerability());

            if (photonView.IsMine)
            {
                if (health > 0)
                {
                    health--;
                }
                if (health == 0)
                {
                    photonView.RPC(RPC_KillParticles, RpcTarget.All);//No need to have it buffered as the player will respawn right away
                    StartCoroutine(Respawn());
                }
            }
        }
    }

    IEnumerator StartInvulnerability()
    {
        isInvulnerable = true;
        col.enabled = false;
        anim.SetBool(AnimHash_Invulnerable, true);
        yield return new WaitForSeconds(invulnerabilityDuration);

        isInvulnerable = false;
        col.enabled = true;
        anim.SetBool(AnimHash_Invulnerable, false);
    }

    //Using OnPhotonSerializeView to make sure all spawned instances share same health value
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        stream.Serialize(ref health);
        UpdateHealthUI();
    }

    IEnumerator Respawn()
    {
        isDead = true;
        yield return new WaitForSeconds(deadDuration);

        health = lives.Length;
        transform.position = GameplayManager.Instance.GetRandomSpawningPosition();
        isDead = false;
    }

    private void OnDestroy()
    {
        if (uiPanel != null)
        {
            Destroy(uiPanel.gameObject); 
        }
    }
}
